FROM mcr.microsoft.com/dotnet/sdk:6.0

WORKDIR /app

COPY . .
RUN dotnet dev-certs https --trust
EXPOSE 5087

ENTRYPOINT ["dotnet", "run"]