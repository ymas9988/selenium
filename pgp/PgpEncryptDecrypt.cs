using System.IO;
using System.Text;
using Org.BouncyCastle.Bcpg.OpenPgp;

public class PgpEncryptDecrypt
{
    public static void EncryptFile(string inputFile, string outputFile, string publicKeyFile, bool armor, bool withIntegrityCheck)
    {
        using (Stream inputStream = File.OpenRead(inputFile))
        using (Stream outputStream = File.Create(outputFile))
        using (Stream publicKeyStream = File.OpenRead(publicKeyFile))
        {
            PgpPublicKey publicKey = ReadPublicKey(publicKeyStream);
            PgpEncryptedDataGenerator encryptedDataGenerator = new PgpEncryptedDataGenerator(SymmetricKeyAlgorithmTag.Aes256, withIntegrityCheck, new SecureRandom());
            encryptedDataGenerator.AddMethod(publicKey);

            using (Stream armoredOutputStream = armor ? new ArmoredOutputStream(outputStream) : outputStream)
            using (Stream encryptedOutputStream = encryptedDataGenerator.Open(armoredOutputStream, new byte[4096]))
            {
                byte[] buffer = new byte[4096];
                int bytesRead;
                while ((bytesRead = inputStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    encryptedOutputStream.Write(buffer, 0, bytesRead);
                }
            }
        }
    }

    public static void DecryptFile(string inputFile, string outputFile, string privateKeyFile, string password)
    {
        using (Stream inputStream = File.OpenRead(inputFile))
        using (Stream outputStream = File.Create(outputFile))
        using (Stream privateKeyStream = File.OpenRead(privateKeyFile))
        {
            PgpPrivateKey privateKey = ReadPrivateKey(privateKeyStream, password);
            PgpObjectFactory objectFactory = new PgpObjectFactory(PgpUtilities.GetDecoderStream(inputStream));
            PgpEncryptedDataList encryptedDataList = objectFactory.NextPgpObject() as PgpEncryptedDataList;
            if (encryptedDataList == null)
            {
                throw new ArgumentException("The input file does not contain any encrypted data.");
            }

            PgpPrivateKeyDataDecryptorFactory decryptorFactory = new PgpPrivateKeyDataDecryptorFactory(privateKey);
            PgpPublicKeyEncryptedData encryptedData = encryptedDataList.GetEncryptedDataObjects().Cast<PgpPublicKeyEncryptedData>().FirstOrDefault(x => x.KeyId == privateKey.KeyId);
            if (encryptedData == null)
            {
                throw new ArgumentException("The input file does not contain any encrypted data for the specified private key.");
            }

            using (Stream decryptedStream = encryptedData.GetDataStream(decryptorFactory))
            {
                byte[] buffer = new byte[4096];
                int bytesRead;
                while ((bytesRead = decryptedStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    outputStream.Write(buffer, 0, bytesRead);
                }
            }
        }
    }

    private static PgpPublicKey ReadPublicKey(Stream publicKeyStream)
    {
        PgpPublicKeyRingBundle publicKeyRingBundle = new PgpPublicKeyRingBundle(PgpUtilities.GetDecoderStream(publicKeyStream));
        PgpPublicKeyRing publicKeyRing = publicKeyRingBundle.GetPublicKeyRings().Cast<PgpPublicKeyRing>().FirstOrDefault();
        if (publicKeyRing == null)
        {
            throw new ArgumentException("The public key file does not contain any public keys.");
        }

        PgpPublicKey publicKey = publicKeyRing.GetPublicKeys().Cast<PgpPublicKey>().FirstOrDefault(x => x.IsEncryptionKey);
        if (publicKey == null)
        {
            throw new ArgumentException("The public key file does not contain any encryption keys.");
        }

        return publicKey;
    }

    private static PgpPrivateKey ReadPrivateKey(Stream privateKeyStream, string password)
    {
        PgpSecretKeyRingBundle privateKeyRingBundle = new PgpSecretKeyRingBundle(PgpUtilities.GetDecoderStream(privateKeyStream));
    PgpSecretKeyRing privateKeyRing = privateKeyRingBundle.GetSecretKeyRings().Cast<PgpSecretKeyRing>().FirstOrDefault();
    if (privateKeyRing == null)
    {
        throw new ArgumentException("The private key file does not contain any private keys.");
    }

    PgpSecretKey secretKey = privateKeyRing.GetSecretKeys().Cast<PgpSecretKey>().FirstOrDefault(x => x.IsSigningKey && x.IsPrivateKey && x.KeyId == privateKeyRing.GetPublicKey().KeyId);
    if (secretKey == null)
    {
        throw new ArgumentException("The private key file does not contain any private keys for the specified public key.");
    }

    return secretKey.ExtractPrivateKey(password.ToCharArray());
}
}
