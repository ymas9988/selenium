string input = "Monsieur Dupont, Madame Martin, Mr Smith, Mme. Dupont, Monsieur. Martin";
string regex = @"(?<=(?:Madame|Monsieur|Mme|M\.|Mr)\s)[A-Za-zÀ-ÖØ-öø-ÿ]+";
List<string> noms = new List<string>();

MatchCollection matches = Regex.Matches(input, regex);
foreach (Match match in matches)
{
    string nom = match.Value;
        noms.Add(nom);
        }

        Console.WriteLine(string.Join(", ", noms));
        

string input = "Monsieur Dupont, Madame Martin, Mr Smith, Mme. Dupont, Monsieur. Martin";
string regex = @"(?<=\s|^)(?:Madame\s|Mr\s|Mme\s|Monsieur\s)(?=\S)";
List<string> prenoms = new List<string>();

MatchCollection matches = Regex.Matches(input, regex);
foreach (Match match in matches)
{
    string prenom = match.Value.TrimEnd('.', ' ');
        prenoms.Add(prenom);
        }

        Console.WriteLine(string.Join(", ", prenoms));
        


string input = "nom : Dupont , prénom : Renée, lastname : Martin-Garcia, firstname : Joséphine, name : Léa, Monsieur toto, Madame Dupond, Mme Martin, Mr Smith";
string regex = @"(?:(?<=Madame\s|Mr\s|Mme\s|Monsieur\s)|nom|pr[eé]nom|lastname|firstname|name)\s*:\s*(?<valeur>[A-Za-zÀ-ÿ\-']+)";
List<string> valeurs = new List<string>();

MatchCollection matches = Regex.Matches(input, regex);
foreach (Match match in matches)
{
    string valeur = match.Groups["valeur"].Value;
        valeurs.Add(valeur);
        }

        Console.WriteLine(string.Join(", ", valeurs));
        
        
public void Encrypt(Stream streamToEncrypt, Stream encryptedStream, PgpPublicKey publicKey, PgpPrivateKey privateKey, char[] passphrase)
{
    // Créer un flux compressé à partir du flux de sortie chiffré
    var compressedStream = new PgpCompressedDataGenerator(CompressionAlgorithmTag.Zip).Open(encryptedStream);

    // Créer un flux de chiffrement à partir du flux compressé
    var encryptedDataGenerator = new PgpEncryptedDataGenerator(SymmetricKeyAlgorithmTag.Cast5, true);
    encryptedDataGenerator.AddMethod(publicKey);
    var encryptedStreamOut = encryptedDataGenerator.Open(compressedStream, new byte[1024]);

    // Créer un flux signé à partir du flux de chiffrement
    var signatureGenerator = new PgpSignatureGenerator(publicKey.Algorithm, HashAlgorithmTag.Sha256);
    signatureGenerator.InitSign(PgpSignature.BinaryDocument, privateKey);
    foreach (string userId in publicKey.GetUserIds())
    {
        var subPacketGenerator = new PgpSignatureSubpacketGenerator();
        subPacketGenerator.SetSignerUserId(false, userId);
        signatureGenerator.SetHashedSubpackets(subPacketGenerator.Generate());
        break;
    }
    signatureGenerator.GenerateOnePassVersion(true).Encode(encryptedStreamOut);
    var signatureStreamOut = new BcpgOutputStream(encryptedStreamOut);
    signatureGenerator.Generate().Encode(signatureStreamOut);

    // Copier les données du flux d'entrée dans le flux de chiffrement
    int length = 0;
    byte[] buffer = new byte[1024];
    while ((length = streamToEncrypt.Read(buffer, 0, buffer.Length)) > 0)
    {
        encryptedStreamOut.Write(buffer, 0, length);
        signatureGenerator.Update(buffer, 0, length);
    }

    // Fermer les flux
    encryptedStreamOut.Close();
    signatureStreamOut.Close();
    compressedStream.Close();
}
