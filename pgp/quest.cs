using System;
using System.IO;
using System.Linq;
using Org.BouncyCastle.Bcpg.OpenPgp;

public static class PGPKeyUtils
{
    public static PgpPublicKey LoadPublicKeyFromAscFile(string ascFilePath)
    {
        using (var inputStream = File.OpenRead(ascFilePath))
        {
            var publicKeyRingCollection = new PgpPublicKeyRingBundle(inputStream);
            var publicKeyRing = publicKeyRingCollection.GetKeyRings().Cast<PgpPublicKeyRing>().FirstOrDefault();
            if (publicKeyRing == null)
            {
                throw new ArgumentException("The specified ASC file does not contain any public keys.");
            }
            var publicKey = publicKeyRing.GetPublicKeys().Cast<PgpPublicKey>().FirstOrDefault();
            if (publicKey == null)
            {
                throw new ArgumentException("The specified ASC file does not contain any public keys.");
            }
            return publicKey;
        }
    }

    public static PgpSecretKey LoadSecretKeyFromKeyStore(string keyStoreFilePath, string keyStorePassword, long keyId)
    {
        using (var inputStream = File.OpenRead(keyStoreFilePath))
        {
            var keyStore = new PgpSecretKeyRingBundle(inputStream);
            var secretKey = keyStore.GetKeyRings()
                .Cast<PgpSecretKeyRing>()
                .SelectMany(r => r.GetSecretKeys().Cast<PgpSecretKey>())
                .FirstOrDefault(k => k.KeyId == keyId);
            if (secretKey == null)
            {
                throw new ArgumentException($"The specified key store does not contain a secret key with ID {keyId}.");
            }
            var privateKey = secretKey.ExtractPrivateKey(keyStorePassword.ToCharArray());
            return secretKey;
        }
    }

    public static void ExportKeyToAscFile(PgpPublicKey publicKey, string ascFilePath)
    {
        using (var outputStream = File.Create(ascFilePath))
        {
            var armoredOutputStream = new ArmoredOutputStream(outputStream);
            publicKey.Encode(armoredOutputStream);
            armoredOutputStream.Close();
        }
    }
}
